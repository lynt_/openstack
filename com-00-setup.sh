#!/bin/bash

source functions.sh
source info.sh

# [1] Install initial packages

function install_initial_packages () {

    echocolor "Update and upgrade"
    sleep 3
    apt update && apt upgrade -y
    echocolor "Done update and upgrade"
    sleep 3

    echocolor "Install initial packages"
    sleep 3
    apt install -y net-tools curl
    echocolor "Done install net-tools package"
    sleep 3
}

function install_crudini () {

    echocolor "Install crudini package"
    sleep 3
    apt install -y crudini
    echocolor "Done install crudini package"
    sleep 3
}

function name_resolution () {

    echocolor "Add name resolution"
    sleep 3
    sed -i "3i$HOST_CTL_IP $HOST_CTL" /etc/hosts
    sed -i "4i$HOST_COM_STR_IP $HOST_COM_STR" /etc/hosts
    echocolor "Done add name resolution"

}

# Check network connection 
function check_net () {

    echocolor "Test network connection"
    sleep 3
    ping 8.8.8.8 -c 3
    sleep 2
    ping $HOST_CTL -c 3
    sleep 2
    ping $HOST_COM_STR -c 3
    sleep 2
    ping google.com -c 3
    sleep 2

    echocolor "Done test network connection"
    sleep 3
}

function install_chrony () {
    echocolor "Set timezone Asia/Ho_Chi_Minh"
	timedatectl set-timezone Asia/Ho_Chi_Minh
	sleep 3

	echocolor "Install NTP Service"
	sleep 3

	apt-get install -y chrony
	ntp_file=/etc/chrony/chrony.conf
	ntp_bak_file="/etc/chrony/chrony.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

	# Backup config file
	cp $ntp_file $ntp_bak_file

    sed -i '1s/^/server controller iburst\n/' $ntp_file

    service chrony restart

	timedatectl

	echocolor "Done install ntp service"

}

#  	Configure Openstack Zed repository. 
function install_ops () {
    apt -y install software-properties-common
    add-apt-repository cloud-archive:zed
    apt update
    apt -y upgrade 
}


###########################################
########   RUN   ##########################
###########################################

install_initial_packages
install_crudini
name_resolution
check_net
install_chrony
install_ops
