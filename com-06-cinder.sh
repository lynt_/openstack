#!/bin/bash


source functions.sh
source info.sh

####################################################
####### Configure Cinder (Storage node)#############
####################################################


# [1] 	Install Cinder Volume service.

function install_cinder () {
    echocolor "Install Cinder Volume service."
    sleep 3
    apt -y install cinder-volume python3-mysqldb 
    echocolor "Done Install Cinder Volume service."
    sleep 3
}

# [2] 	Configure Cinder Volume.

function config_cinder () {
    echocolor "Configure cinder"
    sleep 3
    cinder_config=/etc/cinder/cinder.conf
    cinder_config_bak="/etc/cinder/cinder.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $cinder_config $cinder_config_bak
    touch $cinder_config
    
    ## DEFAULT 
    ops_add $cinder_config DEFAULT my_ip $HOST_COM_STR_IP
    ops_add $cinder_config DEFAULT rootwrap_config /etc/cinder/rootwrap.conf
    ops_add $cinder_config DEFAULT api_paste_confg /etc/cinder/api-paste.ini
    ops_add $cinder_config DEFAULT state_path $STATE_PATH_CINDER
    ops_add $cinder_config DEFAULT auth_strategy keystone
    ops_add $cinder_config DEFAULT transport_url rabbit://$RABBIT_USER:$RABBIT_PASS@$HOST_CTL
    ops_add $cinder_config DEFAULT enable_v3_api True
    ops_add $cinder_config DEFAULT glance_api_servers http://$HOST_CTL:9292
    ops_add $cinder_config DEFAULT enabled_backends lvm

    ## database
    ops_add $cinder_config database connection mysql+pymysql://$MYSQL_CINDER_USER:$MYSQL_CINDER_USER_PASS@$HOST_CTL/cinder

    ## keystone_authtoken
    ops_add $cinder_config keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
    ops_add $cinder_config keystone_authtoken auth_url http://$HOST_CTL:5000
    ops_add $cinder_config keystone_authtoken memcached_servers $HOST_CTL:11211
    ops_add $cinder_config keystone_authtoken auth_type password
    ops_add $cinder_config keystone_authtoken project_domain_name default
    ops_add $cinder_config keystone_authtoken user_domain_name default
    ops_add $cinder_config keystone_authtoken project_name service
    ops_add $cinder_config keystone_authtoken username $CINDER_USER
    ops_add $cinder_config keystone_authtoken password $CINDER_USER_PASS

    ## oslo_concurrency
    ops_add $cinder_config oslo_concurrency lock_path $STATE_PATH_CINDER/tmp

    chmod 640 $cinder_config
    chgrp cinder $cinder_config
    systemctl restart cinder-volume 
    systemctl enable cinder-volume 
}

#=======================================================

########################################################
###### Use Cinder Storage (LVM)     ####################
########################################################

#[1] 	Create a volume group for Cinder on Storage Node. 
### CAN SUA CHO PHU HOP

function create_volume_group () {
    pvcreate /dev/sda4
    vgcreate -s 32M vg_volume01 /dev/sda4
}

# [2] 	Configure Cinder Volume on Storage Node.

function config_cinder_lvm () {
    echocolor "Configure Cinder Volume on Storage Node."
    sleep 3
    apt -y install targetcli-fb python3-rtslib-fb 
    cinder_config=/etc/cinder/cinder.conf  

    ops_add $cinder_config lvm target_helper lioadm
    ops_add $cinder_config lvm target_protocol iscsi
    ops_add $cinder_config lvm target_ip_address \$my_ip
    # volume group name created on [1]
    ops_add $cinder_config lvm volume_group vg_volume01
    ops_add $cinder_config lvm volume_driver cinder.volume.drivers.lvm.LVMVolumeDriver
    ops_add $cinder_config lvm volumes_dir $STATE_PATH_CINDER/volumes

    systemctl restart cinder-volume 
    echocolor "Done Configure Cinder Volume on Storage Node."
    sleep 3
}
#[3] 	Configure Nova on Compute Node

function config_nova () {
    echocolor "	Configure Nova on Compute Node"
    sleep 3
    nova_config=/etc/nova/nova.conf 

    ops_add $nova_config cinder os_region_name RegionOne 
    systemctl restart nova-compute 

    echocolor "Done	Configure Nova on Compute Node"
    sleep 3
}


#[1] install cinder volume
install_cinder
# [2] Configure cinder 
config_cinder

#create-volume-group
create_volume_group
#config cinder (LVM)
config_cinder_lvm
#config nova
config_nova
