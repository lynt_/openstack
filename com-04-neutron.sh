#!/bin/bash

source functions.sh
source info.sh

#[1] 	Install Neutron services on Compute Node.

function install_neutron () {
    echocolor "Install Neutron services on Compute Node."
    sleep 3

    apt -y install neutron-common neutron-plugin-ml2 neutron-ovn-metadata-agent ovn-host openvswitch-switch 

    echocolor "Done Install Neutron services on Compute Node."
    sleep 3
}

#[2] 	Configure Neutron services. 

function config_neutron () {
    echocolor "Configure Neutron Services"
    sleep 3

    echocolor "Configure neutron.conf file"
    sleep 3

    neutron_config=/etc/neutron/neutron.conf
    neutron_config_bak="/etc/neutron/neutron.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

    mv $neutron_config $neutron_config_bak
    touch /etc/neutron/neutron.conf

    ## DEFAULT
    ops_add $neutron_config DEFAULT core_plugin ml2
    ops_add $neutron_config DEFAULT service_plugins ovn-router
    ops_add $neutron_config DEFAULT auth_strategy keystone
    ops_add $neutron_config DEFAULT state_path $STATE_PATH_NEUTRON
    ops_add $neutron_config DEFAULT allow_overlapping_ips True
    ops_add $neutron_config DEFAULT transport_url rabbit://$RABBIT_USER:$RABBIT_PASS@$HOST_CTL

    ## keystone_authtoken
    ops_add $neutron_config keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
    ops_add $neutron_config keystone_authtoken auth_url http://$HOST_CTL:5000
    ops_add $neutron_config keystone_authtoken memcached_servers $HOST_CTL:11211
    ops_add $neutron_config keystone_authtoken auth_type password
    ops_add $neutron_config keystone_authtoken project_domain_name default
    ops_add $neutron_config keystone_authtoken user_domain_name default
    ops_add $neutron_config keystone_authtoken project_name service
    ops_add $neutron_config keystone_authtoken username $NEUTRON_USER
    ops_add $neutron_config keystone_authtoken password $NEUTRON_USER_PASS

    ## oslo_concurrency
    ops_add $neutron_config oslo_concurrency lock_path $STATE_PATH_NEUTRON/lock

    chmod 640 $neutron_config

    chgrp neutron $neutron_config 

    echocolor "Done Configure neutron.conf file"
    sleep 3
    
}

function config_neutron_ml2 () {

    echocolor "Configure ml2_conf.ini file"
    sleep 3

    ml2_config=/etc/neutron/plugins/ml2/ml2_conf.ini
    ml2_config_bak="/etc/neutron/plugins/ml2/ml2_conf.ini.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $ml2_config $ml2_config_bak
    touch /etc/neutron/plugins/ml2/ml2_conf.ini

    ## DEFAULT
    ops_add $ml2_config DEFAULT debug false

    ## ml2
    ops_add $ml2_config ml2 type_drivers flat,geneve
    ops_add $ml2_config ml2 tenant_network_types geneve
    ops_add $ml2_config ml2 mechanism_drivers ovn
    ops_add $ml2_config ml2 extension_drivers port_security
    ops_add $ml2_config ml2 overlay_ip_version 4   

    ## ml2_type_geneve
    ops_add $ml2_config ml2_type_geneve  vni_ranges 1:65536
    ops_add $ml2_config ml2_type_geneve max_header_size 38

    ## [ml2_type_flat]
    ops_add $ml2_config ml2_type_flat flat_networks *

    ## securitygroup
    ops_add $ml2_config securitygroup enable_security_group True
    ops_add $ml2_config securitygroup firewall_driver neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver

    ## ovn
    ops_add $ml2_config ovn ovn_nb_connection tcp:$HOST_CTL_IP:6641
    ops_add $ml2_config ovn ovn_sb_connection tcp:$HOST_CTL_IP:6642
    ops_add $ml2_config ovn ovn_l3_scheduler leastloaded
    ops_add $ml2_config ovn ovn_metadata_enabled True

    chmod 640 $ml2_config
    chgrp neutron $ml2_config

    echocolor "Done Configure ml2_conf.ini file"
    sleep 3

}

function config_neutron_ovn_metadata () {
    echocolor "Configure neutron_ovn_metadata_agent.ini file"
    sleep 3

    ovn_config=/etc/neutron/neutron_ovn_metadata_agent.ini 
    ovn_config_bak="/etc/neutron/neutron_ovn_metadata_agent.ini.bak_$(date "+%d%m%Y")_$(date "+%T")"

    mv $ovn_config $ovn_config_bak
    grep -vE "^$|^#" $ovn_config_bak > $ovn_config

    ## [DEFAULT]
 
    ops_add $ovn_config DEFAULT nova_metadata_host $HOST_CTL
    ops_add $ovn_config DEFAULT nova_metadata_protocol http

    ops_add $ovn_config DEFAULT metadata_proxy_shared_secret metadata_secret

    #[ovs]
    ops_add $ovn_config ovs ovsdb_connection tcp:127.0.0.1:6640

    #[agent]
    ops_add $ovn_config agent root_helper "sudo neutron-rootwrap /etc/neutron/rootwrap.conf"

    #[ovn]
    # IP address of Network node
    ops_add $ovn_config ovn ovn_sb_connection tcp:$HOST_CTL_IP:6642

    echocolor "Done Configure neutron_ovn_metadata_agent.ini file"
    sleep 3

}

function config_neutron_switch () {
    echocolor "Configure /etc/default/openvswitch-switch file"
    sleep 3

    switch_config=/etc/default/openvswitch-switch 
    switch_config_bak="/etc/default/openvswitch-switch.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $switch_config $switch_config_bak
    touch /etc/default/openvswitch-switch

    # sudo sed -i "8s/.*/OVS_CTL_OPTS=\"--ovsdb-server-options='--remote=ptcp:6640:$HOST_COM_STR_IP'\"/" $switch_config
    echo "OVS_CTL_OPTS=\"--ovsdb-server-options='--remote=ptcp:6640:127.0.0.1'\"" >> $switch_config


    echocolor "Done Configure /etc/default/openvswitch-switch file"
    sleep 3

}

function config_neutron_nova() {
    echocolor "Configure /etc/nova/nova.conf file"
    sleep 3

    nova_config=/etc/nova/nova.conf
    nova_config_bak="/etc/nova/nova.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

    cp $nova_config $nova_config_bak
    

    ## DEFAULT
    ops_add $nova_config DEFAULT vif_plugging_is_fatal True
    ops_add $nova_config DEFAULT vif_plugging_timeout 300

    ## neutron
    ops_add $nova_config neutron auth_url http://$HOST_CTL:5000
    ops_add $nova_config neutron auth_type password
    ops_add $nova_config neutron project_domain_name default
    ops_add $nova_config neutron user_domain_name default
    ops_add $nova_config neutron region_name RegionOne
    ops_add $nova_config neutron project_name service
    ops_add $nova_config neutron username $NEUTRON_USER
    ops_add $nova_config neutron password $NEUTRON_USER_PASS
    ops_add $nova_config neutron service_metadata_proxy True
    ops_add $nova_config neutron metadata_proxy_shared_secret metadata_secret
    ops_add $nova_config neutron insecure false 

    echocolor "Done Configure /etc/nova/nova.conf file"
    sleep 3
}


# [3] 	Start Neutron services. 

function start_neutron () {
    echocolor "Start Neutron services. "
    sleep 3
    systemctl restart openvswitch-switch ovn-controller ovn-host

    ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini

    systemctl restart neutron-ovn-metadata-agent

    systemctl restart nova-compute
    ovs-vsctl set open . external-ids:ovn-remote=tcp:$HOST_CTL_IP:6642

    ovs-vsctl set open . external-ids:ovn-encap-type=geneve

    ovs-vsctl set open . external-ids:ovn-encap-ip=$HOST_COM_STR_IP 

    echocolor "Done Start Neutron services. "
    sleep 3   
}


################################################
##########   RUN   #############################

# [1] 	Install Neutron services on Compute Node.
install_neutron
# [2] 	Configure Neutron services. 
config_neutron
config_neutron_ml2
config_neutron_ovn_metadata
config_neutron_switch
config_neutron_nova
# [3] 	Start Neutron services.
start_neutron
