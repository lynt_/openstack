#!/bin/bash

source functions.sh
source info.sh

# [1] Add a User and Database on MariaDB for Keystone.

function add_user () {
    echocolor "Add a User and Database on MariaDB for Keystone."
    sleep 3 

    cat << EOF | mysql
CREATE DATABASE keystone;
GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' \
IDENTIFIED BY '$MYSQL_KEYSTONE_USER_PASS';
GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%' \
IDENTIFIED BY '$MYSQL_KEYSTONE_USER_PASS';
FLUSH PRIVILEGES;
EOF
	echocolor "Done create database for keystone service"
    sleep 3
}



# [2] Install Keystone.

function install_keystone () {
    echocolor "Install Keystone"
    sleep 3
    apt -y install keystone python3-openstackclient apache2 libapache2-mod-wsgi-py3 python3-oauth2client 
    echocolor "Done Install Keystone"
    sleep 3
}

#[3] Configure Keystone. 

function config_keystone () {
    echocolor "Configure Keystone"
    sleep 3

    keystone_config=/etc/keystone/keystone.conf 
    keystone_config_bak="/etc/keystone/keystone.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $keystone_config $keystone_config_bak
    grep -vE "^$|^#" $keystone_config_bak > $keystone_config

    sed -i "444s/.*/memcache_servers = $HOST_CTL_IP:11211/" $keystone_config

    ops_add $keystone_config database connection mysql+pymysql://keystone:password@$HOST_CTL_IP/keystone



    su -s /bin/bash keystone -c "keystone-manage db_sync" 
    
    # initialize Fernet key
    keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone 
    keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

    keystone-manage bootstrap --bootstrap-password $BOOTSTRAP_PASS \
    --bootstrap-admin-url http://$HOST_CTL:5000/v3/ \
    --bootstrap-internal-url http://$HOST_CTL:5000/v3/ \
    --bootstrap-public-url http://$HOST_CTL:5000/v3/ \
    --bootstrap-region-id RegionOne 

}


#[4] Configure Apache httpd. 

function config_apache () {
    echocolor "Configure Apache httpd."
    sleep 3
    sudo sed -i "70i\ServerName $HOST_CTL" /etc/apache2/apache2.conf
    systemctl restart apache2 
    echocolor "Configure Apache httpd."
    sleep 3
}

# [5]  	Load environment variables

function load_variables () {
    echocolor "Load environment variables" 
    sleep 3
    cd /root
    touch keystonerc
    file="keystonerc"
    echo "export OS_PROJECT_DOMAIN_NAME=default" >> $file
    echo "export OS_USER_DOMAIN_NAME=default" >> $$file
    echo "export OS_PROJECT_NAME=admin" >> $file
    echo "export OS_USERNAME=admin" >> $file
    echo "export OS_PASSWORD=$BOOTSTRAP_PASS" >> $file
    echo "export OS_AUTH_URL=http://$HOST_CTL:5000/v3" >> $file
    echo "export OS_IDENTITY_API_VERSION=3" >> $file
    echo "export OS_IMAGE_API_VERSION=2" >> $file
    echo "export PS1='\u@\h \W(keystone)\$ '" >> $file


    chmod 600 ~/keystonerc 
    source ~/keystonerc 
    echo "source ~/keystonerc " >> ~/.bashrc 

    echocolor "Done Load environment variables" 
    sleep 3
}


# [6] Create projects

function create_project () {
    echocolor "Create openstack project"
    sleep 3
    openstack project create --domain default --description "Service Project" service 
    echocolor "Created openstack project"
    sleep 3

    echocolor "Check openstack projects list"
    sleep 3
    openstack project list 
    echocolor "DONE INSTALL KEYSTONE"
    sleep 3
}


##################################################
############   RUN     ###########################
##################################################


# [1] Add a User and Database on MariaDB for Keystone.
add_user
# [2] 	Install Keystone.
install_keystone
# [3] 	Configure Keystone.
config_keystone
# [4] Configure Apache httpd. 
config_apache
# [5]  	Load environment variables
load_variables
# [6] Create projects
create_project

