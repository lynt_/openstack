#!/bin/bash

source functions.sh
source info.sh

# [1] 	Add user or service for Neutron on Keystone.

function neutron_register () {
    echocolor "Add user or service for Neutron on Keystone."
    sleep 3

    openstack user create --domain default --project service --password $NEUTRON_USER_PASS $NEUTRON_USER
    openstack role add --project service --user $NEUTRON_USER admin  
    openstack service create --name neutron --description "OpenStack Networking service" network
    openstack endpoint create --region RegionOne network public http://$HOST_CTL:9696
    openstack endpoint create --region RegionOne network internal http://$HOST_CTL:9696
    openstack endpoint create --region RegionOne network admin http://$HOST_CTL:9696

    echocolor "Done add user or service for Neutron on Keystone."
    sleep 3
}

# [2]  	Add a User and Database on MariaDB for Neutron

function add_user () {

    echocolor "Add a User and Database on MariaDB for Neutron"
    sleep 3
        cat << EOF | mysql
CREATE DATABASE neutron;
GRANT ALL PRIVILEGES ON neutron.* TO neutron@'localhost' IDENTIFIED BY '$MYSQL_NEUTRON_USER_PASS';
GRANT ALL PRIVILEGES ON neutron.* TO neutron@'%' IDENTIFIED BY '$MYSQL_NEUTRON_USER_PASS';
FLUSH PRIVILEGES;
EOF
	echocolor "Done Add a User and Database on MariaDB for Neutron"
    sleep 3
}

# [3] Install required packages for Network Node.

function install_neutron() {
    echocolor "Install neutron"
    sleep 3
    apt -y install neutron-server neutron-plugin-ml2 python3-neutronclient ovn-central openvswitch-switch nginx libnginx-mod-stream 
    echocolor "Done install neutron"
    sleep 3
}


# [4] 	Configure Neutron Server

function config_neutron () {
    echocolor "Configure Neutron"
    sleep 3

    echocolor "Configrure neutron.conf file"
    sleep 3

    neutron_config=/etc/neutron/neutron.conf
    neutron_config_bak="/etc/neutron/neutron.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

    mv $neutron_config $neutron_config_bak
    cd /etc/neutron
    touch neutron.conf

    ## database
    ops_add $neutron_config database connection mysql+pymysql://$MYSQL_NEUTRON_USER:$MYSQL_NEUTRON_USER_PASS@controller/neutron

    ## DEFAULT
    ops_add $neutron_config DEFAULT bind_host $HOST_CTL_IP
    ops_add $neutron_config DEFAULT bind_port 9696
    ops_add $neutron_config DEFAULT core_plugin ml2
    ops_add $neutron_config DEFAULT service_plugins ovn-router
    ops_add $neutron_config DEFAULT auth_strategy keystone
    ops_add $neutron_config DEFAULT state_path $STATE_PATH_NEUTRON
    ops_add $neutron_config DEFAULT allow_overlapping_ips True
    ops_add $neutron_config DEFAULT transport_url rabbit://$RABBIT_USER:$RABBIT_PASS@$HOST_CTL
    ### configure Networking to notify Compute of network topology changes:
    ops_add $neutron_config DEFAULT notify_nova_on_port_status_changes true
    ops_add $neutron_config DEFAULT notify_nova_on_port_data_changes true

    ## keystone_authtoken
    ops_add $neutron_config keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
    ops_add $neutron_config keystone_authtoken auth_url http://$HOST_CTL:5000
    ops_add $neutron_config keystone_authtoken memcached_servers $HOST_CTL:11211
    ops_add $neutron_config keystone_authtoken auth_type password
    ops_add $neutron_config keystone_authtoken project_domain_name default
    ops_add $neutron_config keystone_authtoken user_domain_name default
    ops_add $neutron_config keystone_authtoken project_name service
    ops_add $neutron_config keystone_authtoken username $NEUTRON_USER
    ops_add $neutron_config keystone_authtoken password $NEUTRON_USER_PASS

    ## nova
    ops_add $neutron_config nova auth_url http://$HOST_CTL:5000
    ops_add $neutron_config nova auth_type password
    ops_add $neutron_config nova project_domain_name default
    ops_add $neutron_config nova user_domain_name default
    ops_add $neutron_config nova region_name RegionOne
    ops_add $neutron_config nova project_name service
    ops_add $neutron_config nova username $NOVA_USER
    ops_add $neutron_config nova password $NOVA_USER_PASS

    ## 0slo_concurrency
    ops_add $neutron_config oslo_concurrency lock_path $STATE_PATH_NEUTRON/tmp

    chmod 640 $neutron_config 
    chgrp neutron $neutron_config 

    echocolor "Done Configrure neutron.conf file"
    sleep 3

}
function config_neutron_ml2 () {
    
    echocolor "Done Configrure ml2_conf.ini file"
    sleep 3

    ml2_config=/etc/neutron/plugins/ml2/ml2_conf.ini
    ml2_config_bak="/etc/neutron/plugins/ml2/ml2_conf.ini.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $ml2_config $ml2_config_bak
    touch /etc/neutron/plugins/ml2/ml2_conf.ini

    # [DEFAULT]
    ops_add $ml2_config DEFAULT debug false

    # [ml2]
    ops_add $ml2_config ml2 type_drivers flat,geneve
    ops_add $ml2_config ml2 tenant_network_types geneve
    ops_add $ml2_config ml2 mechanism_drivers ovn
    ops_add $ml2_config ml2 extension_drivers port_security
    ops_add $ml2_config ml2 overlay_ip_version 4

    # [ml2_type_geneve]
    ops_add $ml2_config ml2_type_geneve vni_ranges 1:65536
    ops_add $ml2_config ml2_type_geneve max_header_size 38

    # [ml2_type_flat]
    ops_add $ml2_config ml2_type_flat flat_networks *

    # [securitygroup]
    ops_add $ml2_config securitygroup enable_security_group True
    ops_add $ml2_config securitygroup firewall_driver neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver

    # [ovn]
    # IP address of this Network node
    ops_add $ml2_config ovn ovn_nb_connection tcp:$HOST_CTL_IP:6641
    ops_add $ml2_config ovn ovn_sb_connection tcp:$HOST_CTL_IP:6642
    ops_add $ml2_config ovn ovn_l3_scheduler leastloaded
    ops_add $ml2_config ovn ovn_metadata_enabled True

    chmod 640 $ml2_config

    chgrp neutron $ml2_config 

    echocolor "Done Configure ml2_conf.ini file"
    sleep 3

}

function config_neutron_switch () {
    echocolor "Configure /etc/default/openvswitch-switch file"
    sleep 3

    switch_config=/etc/default/openvswitch-switch 
    switch_config_bak="/etc/default/openvswitch-switch.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $switch_config $switch_config_bak
    touch /etc/default/openvswitch-switch 

    echo OVS_CTL_OPTS="--ovsdb-server-options='--remote=ptcp:6640:127.0.0.1'" > $switch_config

    echocolor "Done Configure /etc/default/openvswitch-switch file"
    sleep 3
}

# [5] 	Start Neutron services. 

function start_neutron () {
    systemctl restart openvswitch-switch

    ovs-vsctl add-br br-int
    ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini

    su -s /bin/bash neutron -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugin.ini upgrade head"
    systemctl restart ovn-central ovn-northd

    ovn-nbctl set-connection ptcp:6641:$HOST_CTL_IP -- set connection . inactivity_probe=60000

    ovn-sbctl set-connection ptcp:6642:$HOST_CTL_IP -- set connection . inactivity_probe=60000
    systemctl restart neutron-server nginx    
}


################################################
##########   RUN   #############################
################################################


# [1] 	Add user or service for Neutron on Keystone.
neutron_register
# [2]  	Add a User and Database on MariaDB for Neutron
add_user
# [3] Install required packages for Network Node.
install_neutron
# [4] 	Configure Neutron Server
config_neutron
config_neutron_ml2
config_neutron_switch
# [5] 	Start Neutron services. 
start_neutron





