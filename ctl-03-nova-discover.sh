#!/bin/bash
# [7] Add the compute node to the cell database 

function add_compute_node () {
    echocolor "Add compute node"
    sleep 3
    source ~/keystonerc
    su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova

    ops_add /etc/nova/nova.conf scheduler discover_hosts_in_cells_interval 300
}

#########################################
##### RUN   #############################
#########################################

# [7] Add the compute node to the cell database 
add_compute_node

