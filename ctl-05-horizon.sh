#!/bin/bash


source functions.sh
source info_config.sh


# Function install the packages
horizon_install () {
	echocolor "Install the horizon packages"
	sleep 3
	apt install openstack-dashboard -y
	echocolor "Done install the horizon packages"
}

# Function edit the /etc/openstack-dashboard/local_settings.py file
horizon_config () {
	echocolor "Edit the /etc/openstack-dashboard/local_settings.py file"
	sleep 3

	horizonfile=/etc/openstack-dashboard/local_settings.py
	horizonfilebak=/etc/openstack-dashboard/local_settings.py.bak
	cp $horizonfile $horizonfilebak
	egrep -v "^$|^#" $horizonfilebak > $horizonfile

	sed -i 's/OPENSTACK_HOST = "127.0.0.1"/'"OPENSTACK_HOST = \"$HOST_CTL_IP\""'/g' $horizonfile

	echo "SESSION_ENGINE = 'django.contrib.sessions.backends.cache'" >> $horizonfile
	sed -i "s/'LOCATION': '127.0.0.1:11211',/""'LOCATION': '$HOST_CTL:11211',""/g" $horizonfile
	sed -i 's/OPENSTACK_KEYSTONE_URL = "http:\/\/%s\/identity\/v3" % OPENSTACK_HOST/OPENSTACK_KEYSTONE_URL = "http:\/\/%s:5000\/v3" % OPENSTACK_HOST/g' $horizonfile
	echo "OPENSTACK_KEYSTONE_MULTIDOMAIN_SUPPORT = True" >> $horizonfile
	cat << EOF >> $horizonfile
OPENSTACK_API_VERSIONS = {
    "identity": 3,
    "image": 2,
    "volume": 3,
}
EOF

	echo 'OPENSTACK_KEYSTONE_DEFAULT_DOMAIN = "Default"' >> $horizonfile
	echo 'OPENSTACK_KEYSTONE_DEFAULT_ROLE = "user"' >> $horizonfile

	sed -i 's/TIME_ZONE = "UTC"/TIME_ZONE = "Asia\/Ho_Chi_Minh"/g' $horizonfile
	echocolor "Done edit to horizon config"
}

# Function restart installation
horizon_restart () {
	echocolor "Restart installation"
	sleep 3
	service apache2 reload
}

#######################
###Execute functions###
#######################

# Install the packages
horizon_install

# Edit the /etc/openstack-dashboard/local_settings.py file
horizon_config

# Restart installation
horizon_restart
