#!/bin/bash

source functions.sh
source info.sh

# Function create database for Placement
placement_create_db () {
	echocolor "Create database for Placement Service"
	sleep 3

	cat << EOF | mysql

CREATE DATABASE placement;

GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'localhost' IDENTIFIED BY '$MYSQL_PLACEMENT_USER_PASS';
GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'%' IDENTIFIED BY '$MYSQL_PLACEMENT_USER_PASS';

FLUSH PRIVILEGES;
EOF
	echocolor "Done create database for Placement service"
}


# Function create infomation for Placement service
placement_create_info () {
	echocolor "Set environment variable for user admin"
	source /root/keystonerc
	echocolor "Create infomation for Placement service"
	sleep 3

	## Create info for placement user
	echocolor "Create info for Placement user"
	sleep 3

	openstack user create --domain default --password $PLACEMENT_USER_PASS $PLACEMENT_USER
	openstack role add --project service --user $PLACEMENT_USER admin
	openstack service create --name placement --description "Placement API" placement
	openstack endpoint create --region RegionOne placement public http://$HOST_CTL:8778
	openstack endpoint create --region RegionOne placement internal http://$HOST_CTL:8778
	openstack endpoint create --region RegionOne placement admin http://$HOST_CTL:8778

	echocolor "Done create infomation for Placement service"
}


# Function install components of Placement
placement_install () {
	echocolor "Install placement package"
	sleep 3
	apt-get install -y placement-api
	echocolor "Done install placement package"
}


# Function config /etc/placement/placement.conf file
placement_config () {
	echocolor "Start config placement service"
	placement_file=/etc/placement/placement.conf
	placement_bak_file="/etc/placement/placement.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
	mv $placement_file $placement_bak_file
	touch $placement_config
	
	ops_add $placement_file placement_database connection mysql+pymysql://placement:$MYSQL_PLACEMENT_USER_PASS@$HOST_CTL/placement
		
	ops_add $placement_file api auth_strategy keystone

	ops_add $placement_file keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
	ops_add $placement_file keystone_authtoken auth_url http://$HOST_CTL:5000
	ops_add $placement_file keystone_authtoken memcached_servers $HOST_CTL:11211
	ops_add $placement_file keystone_authtoken auth_type password
	ops_add $placement_file keystone_authtoken project_domain_name default
	ops_add $placement_file keystone_authtoken user_domain_name default
	ops_add $placement_file keystone_authtoken project_name service
	ops_add $placement_file keystone_authtoken username placement
	ops_add $placement_file keystone_authtoken password $PLACEMENT_USER_PASS

	echocolor "Done config placement service"
}


# Function populate the placement database
placement_populate_placement_db () {
	echocolor "Populate the placement database"
	sleep 3
	su -s /bin/sh -c "placement-manage db sync" placement

	echocolor "Restart apache service"
	systemctl restart apache2

	echocolor "Done populate the placement database"
}


# Function verify placement service
placement_verify () {
	echocolor "Verify placement service"
	sleep 3
	source /root/keystonerc
	placement-status upgrade check

	echocolor "Done verity placement service"
}

# Function create database for Nova
nova_create_db () {
	echocolor "Create database for Nova Service"
	sleep 3

	cat << EOF | mysql
CREATE DATABASE nova_api;
CREATE DATABASE nova;
CREATE DATABASE nova_cell0;

GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';
GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';

GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';
GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';

GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';
GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY '$MYSQL_NOVA_USER_PASS';

FLUSH PRIVILEGES;
EOF
	echocolor "Done create database for Nova service"
}

# Function create infomation for Compute service
nova_create_info () {
	echocolor "Set environment variable for user admin"
	source /root/keystonerc
	echocolor "Create infomation for Compute service"
	sleep 3

	## Create info for nova user
	echocolor "Create info for nova user"
	sleep 3

	openstack user create --domain default --password $NOVA_USER_PASS nova
	openstack role add --project service --user nova admin
	openstack service create --name nova --description "OpenStack Compute" compute
	openstack endpoint create --region RegionOne compute public http://$HOST_CTL:8774/v2.1
	openstack endpoint create --region RegionOne compute internal http://$HOST_CTL:8774/v2.1
	openstack endpoint create --region RegionOne compute admin http://$HOST_CTL:8774/v2.1

	echocolor "Done create infomation for Nova service"
}

# Function install components of Nova
nova_install () {
	echocolor "Install nova package"
	sleep 3
	apt-get update -y
	apt-get install -y nova-api nova-conductor nova-scheduler nova-novncproxy placement-api python3-novaclient
	echocolor "Done install nova package"
}

# Function config /etc/nova/nova.conf file
nova_config () {
	echocolor "Start config nova service"
	nova_file=/etc/nova/nova.conf
	nova_bak_file="/etc/nova/nova.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
	cp $nova_file $nova_bak_file
	touch $nova_config

	ops_add $nova_file api_database connection mysql+pymysql://nova:$MYSQL_NOVA_USER_PASS@$HOST_CTL/nova_api
	
	ops_add $nova_file database connection mysql+pymysql://nova:$MYSQL_NOVA_USER_PASS@$HOST_CTL/nova

	ops_add $nova_file DEFAULT transport_url rabbit://openstack:$RABBIT_PASS@$HOST_CTL
	
	ops_add $nova_file api auth_strategy keystone

	ops_add $nova_file keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
	ops_add $nova_file keystone_authtoken auth_url http://$HOST_CTL:5000
	ops_add $nova_file keystone_authtoken memcached_servers $HOST_CTL:11211
	ops_add $nova_file keystone_authtoken auth_type password
	ops_add $nova_file keystone_authtoken project_domain_name default
	ops_add $nova_file keystone_authtoken user_domain_name default
	ops_add $nova_file keystone_authtoken project_name service
	ops_add $nova_file keystone_authtoken username nova
	ops_add $nova_file keystone_authtoken password $NOVA_USER_PASS

	ops_add $nova_file DEFAULT my_ip $HOST_CTL_IP

 
	ops_add $nova_file service_user send_service_user_token true
	ops_add $nova_file service_user auth_url https://controller:5000
	ops_add $nova_file service_user auth_strategy keystone
	ops_add $nova_file service_user auth_type password
	ops_add $nova_file service_user project_domain_name default
	ops_add $nova_file service_user project_name service
	ops_add $nova_file service_user user_domain_name default
	ops_add $nova_file service_user username nova
	ops_add $nova_file service_user password $NOVA_USER_PASS

	ops_add $nova_file vnc enabled true
	ops_add $nova_file vnc server_listen \$my_ip
	ops_add $nova_file vnc server_proxyclient_address \$my_ip

	ops_add $nova_file glance api_servers http://$HOST_CTL:9292

	ops_add $nova_file oslo_concurrency lock_path /var/lib/nova/tmp

	ops_del $nova_file DEFAULT log_dir

	ops_del $nova_file placement os_region_name
	ops_add $nova_file placement os_region_name RegionOne
	ops_add $nova_file placement project_domain_name Default
	ops_add $nova_file placement project_name service
	ops_add $nova_file placement auth_type password
	ops_add $nova_file placement user_domain_name Default
	ops_add $nova_file placement auth_url http://$HOST_CTL:5000
	ops_add $nova_file placement username placement
	ops_add $nova_file placement password $PLACEMENT_USER_PASS

	ops_add $nova_file neutron auth_url  http://$HOST_CTL:5000
	ops_add $nova_file neutron auth_type  password
	ops_add $nova_file neutron project_domain_name  default
	ops_add $nova_file neutron user_domain_name  default
	ops_add $nova_file neutron region_name  RegionOne
	ops_add $nova_file neutron project_name  service
	ops_add $nova_file neutron username  neutron
	ops_add $nova_file neutron password  $NEUTRON_USER_PASS


	ops_add $nova_file wsgi api_paste_config "/etc/nova/api-paste.ini"
	echocolor "Done config nova service"
}

# Function populate the nova-api database
nova_populate_nova-api_db () {
	echocolor "Populate the nova-api database"
	sleep 3
	su -s /bin/sh -c "nova-manage api_db sync" nova
	echocolor "Done populate the nova-api database"
}

# Function register the cell0 database
nova_register_cell0 () {
	echocolor "Register the cell0 database"
	sleep 3
	su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
	echocolor "Done register the cell0 database"
}

# Function create the cell1 cell
nova_create_cell1 () {
	echocolor "Create the cell1 cell"
	sleep 3
	su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
	echocolor "Done create the cell1 cell"
}

# Function populate the nova database
nova_populate_nova_db () {
	echocolor "Populate the nova database"
	sleep 3
	su -s /bin/sh -c "nova-manage db sync" nova
	echocolor "Done populate the nova database"
}

# Function verify nova cell0 and cell1 are registered correctly
nova_verify_cell () {
	echocolor "Verify nova cell0 and cell1 are registered correctly"
	sleep 3
	su -s /bin/sh -c "nova-manage cell_v2 list_cells" nova
	echocolor "Done verity register nova cell0/cell1"
}

# Function restart installation
nova_restart () {
	echocolor "Finalize installation"
	sleep 3

	service nova-api restart
	service nova-scheduler restart
	service nova-conductor restart
	service nova-novncproxy restart
	service apache2 restart
	sleep 3
	
	source /root/keystonerc
	openstack compute service list
	echocolor "Done finalize installation nova service"
}

#######################
###Execute functions###
#######################

# Create database for Placement
placement_create_db

# Create infomation for Placement service
placement_create_info

# Install components of Placement
placement_install

# Config /etc/placement/placement.conf file
placement_config

# Populate the placement database
placement_populate_placement_db

# Verify placement service
placement_verify

# Create database for Nova
nova_create_db

# Create infomation for Compute service
nova_create_info

# Install components of Nova
nova_install

# Config /etc/nova/nova.conf file
nova_config

# Populate the nova-api database
nova_populate_nova-api_db

# Register the cell0 database
nova_register_cell0

# Create the cell1 cell
nova_create_cell1

# Populate the nova database
nova_populate_nova_db

# Verify nova cell0 and cell1 are registered correctly
nova_verify_cell

# Restart installation
nova_restart
