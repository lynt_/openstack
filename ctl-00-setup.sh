#!/bin/bash

source functions.sh 
source info.sh

################################################
################################################

### INITIAL SETUP 

###############################################
###############################################

# [1] Initial setup 
function install_net_tools () {

    echocolor "Update and upgrade"
    sleep 3
    apt update && apt upgrade -y
    echocolor "Done update and upgrade"
    sleep 3

    echocolor "Install initial packages"
    sleep 3
    apt install -y net-tools curl
    echocolor "Done install net-tools package"
    sleep 3
}

# [2] Install crudini package
function install_crudini () {

    echocolor "Install crudini package"
    sleep 3
    apt install -y crudini
    echocolor "Done install crudini package"
    sleep 3
}

# [3] Install name resolution 
function name_resolution () {

    echocolor "Add name resolution"
    sleep 3
    sed -i "3i$HOST_CTL_IP $HOST_CTL" /etc/hosts
    sed -i "4i$HOST_COM_STR_IP $HOST_COM_STR" /etc/hosts
    echocolor "Done add name resolution"

}

# [4] Check network connection 
function check_net () {

    echocolor "Test network connection"
    sleep 3
    ping 8.8.8.8 -c 3
    sleep 2
    ping $HOST_CTL -c 3
    sleep 2
    ping $HOST_COM_STR -c 3
    sleep 2
    ping google.com -c 3
    sleep 2

    echocolor "Done test network connection"
    sleep 3
}


# [5] NTP
function install_ntp () {
        echocolor "Set timezone Asia/Ho_Chi_Minh"
	timedatectl set-timezone Asia/Ho_Chi_Minh
	sleep 3

	echocolor "Install NTP Service"
	sleep 3

	apt-get install -y chrony
	ntp_file=/etc/chrony/chrony.conf
	ntp_bak_file="/etc/chrony/chrony.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"

	# Backup config file
	cp $ntp_file $ntp_bak_file

	sed -i 's/pool 2.debian.pool.ntp.org offline iburst/ \
pool 2.debian.pool.ntp.org offline iburst \
server 0.asia.pool.ntp.org iburst \
server 1.asia.pool.ntp.org iburst/g' $ntp_file

	echo "allow $CIDR_MGNT" >> $ntp_file

	service chrony restart

	timedatectl

	echocolor "Done install ntp service"
}
################################################
################################################

### PRE_REQUIREMENTS OPS

###############################################
###############################################


# [1] Install MariaDB Server

function database(){
    echocolor "Install MariaDB server"
    sleep 3

    apt update && apt upgrade -y
    apt -y install mariadb-server 
    systemctl restart mariadb
 
    echocolor "Done install MariaDB server"
    sleep 3

}

# [2] Configure OpenStack Zed repository

function ops_zed () {
    echocolor "Configure OpenStack Zed repository"
    sleep 3
    apt -y install software-properties-common 
    add-apt-repository cloud-archive:zed 
    apt update && apt -y upgrade
    echocolor "Done configure OpenStack Zed repository"
    sleep 3
}

# [3] Install RabbitMQ, Memcached, Nginx

function install_rabbitmq_memcached () {

    echocolor "Install RabbitMQ, Memcached, Nginx"
    sleep 3
    ## Install
    apt -y install rabbitmq-server memcached python3-pymysql nginx libnginx-mod-stream 
    
    #Add user rabbitmq and 
    echocolor "Add rabbitmq's user"
    sleep 3
    rabbitmqctl add_user $RABBIT_USER $RABBIT_PASS 
    rabbitmqctl set_permissions $RABBIT_USER ".*" ".*" ".*" 
    echocolor "Added rabbitmq's user"
    sleep 3

    echocolor "Config mariadb"
    sleep 3
    #Config mariadb
    mariadb_config=/etc/mysql/mariadb.conf.d/50-server.cnf
    mariadb_config_bak="/etc/mysql/mariadb.conf.d/50-server.cnf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    
    mv $mariadb_config $mariadb_config_bak
    grep -vE "^$|^#" $mariadb_config_bak > $mariadb_config

    ops_add $mariadb_config mysqld bind-address $HOST_CTL_IP
    ops_add $mariadb_config mysqld max_connections 500

    echocolor "Done config mariadb"
    sleep 3

    #Config memcached
    echocolor "Config memcached"
    sleep 3
    memcache_config=/etc/memcached.conf
    memcache_config_bak="/etc/memcached.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $memcache_config $memcache_config_bak
    grep -vE "^$|^#" $memcache_config_bak > $memcache_config
    sed -i "s/-l 127\.0\.0\.1/-l $HOST_CTL_IP/g" $memcache_config
    echocolor "Done config memcached"
    sleep 3
    
    unlink /etc/nginx/sites-enabled/default
    ## restart
    systemctl restart mariadb rabbitmq-server memcached nginx 

    echocolor "Done Install RabbitMQ, Memcached, Nginx"

}

##########################################
#########  RUN  ##########################
##########################################

function initial_setup () {
    # [1] Initial setup 
    install_net_tools
    # [2] Install crudini package
    install_crudini
    # [3] Install name resolution 
	name_resolution
    # [4] Install RabbitMQ, Memcached, Nginx
    check_net
}
function pre_requirement () {
    install_chrony
    # [1] Install MariaDB Server
    database
    # [2] Configure OpenStack Zed repository
    ops_zed
    # [3] Install RabbitMQ, Memcached, Nginx
    install_rabbitmq_memcached
}

initial_setup
pre_requirement
