#!/bin/bash

###########################################
#### Set local variables  for scripts #####
###########################################

echocolor "Set local variable for scripts"
sleep 3

CIDR_MGNT=192.168.100.0/24

#controller node
HOST_CTL=controller
HOST_CTL_IP=192.168.100.11

#compute storage node
HOST_COM_STR=compute
HOST_COM_STR_IP=192.168.100.12

# RabbitMQ
RABBIT_USER=openstack
RABBIT_PASS=password

# Mariadb 
MARIADB_PASS=password
## database keystone
MYSQL_KEYSTONE_USER=keystone
MYSQL_KEYSTONE_USER_PASS=$MARIADB_PASS
## database glance
MYSQL_GLANCE_USER=glance
MYSQL_GLANCE_USER_PASS=$MARIADB_PASS
## database nova
MYSQL_NOVA_USER=nova
MYSQL_NOVA_USER_PASS=$MARIADB_PASS
## database neutron
MYSQL_NEUTRON_USER=neutron
MYSQL_NEUTRON_USER_PASS=$MARIADB_PASS
## database cinder 
MYSQL_CINDER_USER=cinder
MYSQL_CINDER_USER_PASS=$MARIADB_PASS
## database placement
MYSQL_PLACEMENT_USER=placement
MYSQL_PLACEMENT_USER_PASS=$MARIADB_PASS



#bootstrap ops
BOOTSTRAP_PASS=adminpassword

#ops-keystone
## glance
GLANCE_USER=glance
GLANCE_USER_PASS=servicepassword
## nova
NOVA_USER=nova
NOVA_USER_PASS=servicepassword
## placement
PLACEMENT_USER=placement
PLACEMENT_USER_PASS=servicepassword
## neutron
NEUTRON_USER=neutron
NEUTRON_USER_PASS=servicepassword
## cinder
CINDER_USER=cinder
CINDER_USER_PASS=servicepassword


#file
## Location of glance image 
GLANCE_IMG_LOCATION=/var/lib/glance/images
STATE_PATH_NOVA=/var/lib/nova
STATE_PATH_CINDER=/var/lib/cinder
STATE_PATH_NEUTRON=/var/lib/neutron
