#!/bin/bash

source functions.sh
source info.sh

#  [1] 	Add a User or Endpoints for Cinder to Keystone on Control Node. 
function cinder_register () {
    echocolor "Add a User or Endpoints for Cinder to Keystone on Control Node."
    sleep 3
    openstack user create --domain default --project service --password $CINDER_USER_PASS $CINDER_USER
    openstack role add --project service --user $CINDER_USER admin 
    openstack service create --name cinderv3 --description "OpenStack Block Storage" volumev3 
    openstack endpoint create --region RegionOne volumev3 public http://$HOST_CTL:8776/v3/
    openstack endpoint create --region RegionOne volumev3 internal http://$HOST_CTL:8776/v3/
    openstack endpoint create --region RegionOne volumev3 admin http://$HOST_CTL:8776/v3/
    echocolor "Done Add a User or Endpoints for Cinder to Keystone on Control Node."
    sleep 3
}

# [2] 	Add a User and Database on MariaDB for Cinder. 
function add_user () {

    echocolor "Create database for cinder service"
    sleep 3
        cat << EOF | mysql
CREATE DATABASE cinder;
GRANT ALL PRIVILEGES ON cinder.* TO cinder@'localhost' IDENTIFIED BY '$MYSQL_CINDER_USER_PASS';
GRANT ALL PRIVILEGES ON cinder.* TO cinder@'%' IDENTIFIED BY '$MYSQL_CINDER_USER_PASS';
FLUSH PRIVILEGES;
EOF
	echocolor "Done create database for cinder service"
    sleep 3
}

# [3] 	Install Cinder Service. 
function install_cinder () {
    echocolor "Install Cinder Service"
    sleep 3
    apt -y install cinder-api cinder-scheduler python3-cinderclient 
    echocolor "Done Install Cinder Service"
    sleep 3
}

# [4] 	Configure Cinder. 
function config_cinder () {
    echocolor "Configure cinder"
    sleep 3
    cinder_config=/etc/cinder/cinder.conf
    cinder_config_bak="/etc/cinder/cinder.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $cinder_config $cinder_config_bak
    touch $cinder_config

    ## DEFAULT 
    ops_add $cinder_config DEFAULT rootwrap_config /etc/cinder/rootwrap.conf
    ops_add $cinder_config DEFAULT api_paste_confg /etc/cinder/api-paste.ini
    ops_add $cinder_config DEFAULT state_path $STATE_PATH_CINDER
    ops_add $cinder_config DEFAULT auth_strategy keystone
    ## RabbitMQ connection info
    ops_add $cinder_config DEFAULT transport_url rabbit://$RABBIT_USER:$RABBIT_PASSWORD@$HOST_CTL
    ops_add $cinder_config DEFAULT enable_v3_api True

    # database
    ops_add $cinder_config database connection mysql+pymysql://$MYSQL_CINDER_USER:$MYSQL_CINDER_USER_PASS@$HOST_CTL/cinder
    # keystone_authtoken
    ops_add $cinder_config keystone_authtoken www_authenticate_uri http://$HOST_CTL:5000
    ops_add $cinder_config keystone_authtoken auth_url http://$HOST_CTL:5000
    ops_add $cinder_config keystone_authtoken memcached_servers $HOST_CTL:11211
    ops_add $cinder_config keystone_authtoken auth_type password
    ops_add $cinder_config keystone_authtoken project_domain_name default
    ops_add $cinder_config keystone_authtoken user_domain_name default
    ops_add $cinder_config keystone_authtoken project_name service
    ops_add $cinder_config keystone_authtoken username $CINDER_USER
    ops_add $cinder_config keystone_authtoken password $CINDER_USER_PASS

    ## oslo_concurrency
    ops_add $cinder_config olso_concurrency lock_path $STATE_PATH_CINDER/tmp

    chmod 640 $cinder_config
    chgrp cinder $cinder_config

    wsgi_config=/etc/apache2/conf-available/cinder-wsgi.conf
    wsgi_config_bak="/etc/apache2/conf-available/cinder-wsgi.conf.bak_$(date "+%d%m%Y")_$(date "+%T")"
    mv $wsgi_config $wsgi_config_bak
    grep -vE "^$|^#" $wsgi_config_bak > $wsgi_config
    sed -i '1d' $wsgi_config
    sed -i "1i\Listen $HOST_CTL_IP:8776" $wsgi_config
    
}

# [5] 	Start Cinder services. 
function start_cinder () {
    echocolor "Start Cinder services."
    sleep 3

    su -s /bin/bash cinder -c "cinder-manage db sync"
    systemctl restart cinder-scheduler apache2 nginx
    systemctl enable cinder-scheduler
    echo "export OS_VOLUME_API_VERSION=3" >> ~/keystonerc
    source ~/keystonerc

    echocolor "Start Cinder services."
    sleep 3 

    echocolor "verify status"
    sleep 3
    openstack volume service list 
    echocolor "DONE"
    sleep 3
}

#########################################
#########   RUN   #######################
#########################################
#  [1] 	Add a User or Endpoints for Cinder to Keystone on Control Node. 
cinder_register
# [2] 	Add a User and Database on MariaDB for Cinder. 
add_user
# [3] 	Install Cinder Service. 
install_cinder
# [4] 	Configure Cinder. 
config_cinder
# [5] start Cinder services. 
start_cinder

